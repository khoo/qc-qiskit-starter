Basic Quantum Computing with Qiskit
=====

These notebooks collect basic examples experimenting with the [IBM Qiskit](https://qiskit.org/) python libraries.

Installation
-----

Qiskit can be installed locally with `pip`. To insulate the installation from your OS's default environment, it helps to create a virtual environment, e.g.
```
conda create -n qiskit python=3
conda activate qiskit
"pip install qiskit[visualisation]"
 # We want additional visualisation features for e.g. Jupyter
 # Quotes for zsh
```
Other installation presents can be determined from the [Quick start](https://qiskit.org/overview/#quick-start) page.

Running the notebooks
-----

After cloning this repository, it suffices to do:
```
conda activate qiskit
jupyter notebook
```
in a directory containing the checked out directory. The directory structure is navigable in Jupter, and the notebooks can be run.

Getting credentials for IBM Quantum Experience
-----

The instructions for setting up the credentials to access the IBM hardware are at [https://github.com/Qiskit/qiskit-ibmq-provider]. Basically:
1. Go to [https://quantum-computing.ibm.com] and create an account, then copy your API token from the website.
2. Run (e.g. in interactive python):
```
from qiskit import IBMQ
IBMQ.save_account('MY_API_TOKEN')
```
3. Whenever you want to use the IMBQ systems, do:
```
from qiskit import IBMQ

provider = IBMQ.load_account()
backend = provider.get_backend('THE BACKEND TO USE')
```
